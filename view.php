<?php
ob_start ();
session_start ();
require 'connectdb.php';
$post_id = $_GET ["post_id"];
$cuid = $_SESSION ["uid"];
// echo $post_id;
global $mysqli;
$stmt = $mysqli->prepare ( "select story.user_id, title, story, user.username, story.create_date, link
		from story join user on (story.user_id=user.user_id) where story.is_delete=0
		and post_id=?" );
$stmt->bind_param ( 'i', $post_id );
$stmt->execute ();
$stmt->bind_result ( $puid, $title, $story, $username, $date, $link );
$stmt->fetch ();
$stmt->close ();

$stmt = $mysqli->prepare ( "select up, down from rate where post_id=?" );
$stmt->bind_param ( 'i', $post_id );
$stmt->execute ();
$stmt->bind_result ( $up, $down );
$stmt->fetch ();
$stmt->close ();
if ($_SERVER ["REQUEST_METHOD"] == "POST") {
	if ($_SESSION ['token'] !== $_POST ['token']) {
		die ( "\n Request forgery detected" );
	}
}
?>
<!DOCTYPE HTML>
<html>
<head>
<title><?php echo $title;?></title>
</head>
<body>

	<!-- Show post -->
	<h1><?php echo $title;?></h1>
	<h6>Author: <?php echo $username;?></h6>
	<span>Date: <?php echo substr($date,0,4)."/".substr($date, 4,2)."/".substr($date, 6,2);?></span>
	<h6><?php echo "<a href=\"".$link."\">Reference</a>";?></h6>
	<p>
<?php echo $story;?>
</p>
<?php

if ($cuid == $puid) {
	echo "<a href=\"editpost.php?post_id=" . $post_id . "\">" . "Manage my story" . "</a>";
}
?>
<!-- show rate -->
	<form method="post"
		action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]."?post_id=".$post_id); ?>">
<?php echo "UP: ".$up;?>
<button type="submit" name="up" value="up">UP</button>
		<br>
<?php echo "DOWN: ".$down;?>
<button type="submit" name="down" value="down">DOWN</button>
<input type="hidden"
	name="token" value="<?php echo $_SESSION['token'];?>" />
	</form>
	<!-- rate the story -->
<?php
if (isset ( $_POST ["up"] )) {
	if ($_SESSION ["uid"] == null) {
		echo "You are not logged in.";
		echo "<a href=\"login.php\">Log in</a>";
		exit ();
	}
	require 'connectdb.php';
	global $mysqli;
	if ($up == 0 && $down == 0) {
		$stmt = $mysqli->prepare ( "insert into rate (up,post_id) values (?,?)" );
	} else {
		$stmt = $mysqli->prepare ( "update rate set up=? where post_id=?" );
	}
	if (! $stmt) {
		printf ( "Query Prep Failed: %s\n", $mysqli->error );
		exit ();
	}
	$up ++;
	$stmt->bind_param ( 'ii', $up, $post_id );
	$stmt->execute ();
	$stmt->close ();
	header ( "Location: view.php" . "?post_id=" . $post_id );
}

if (isset ( $_POST ["down"] )) {
	if ($_SESSION ["uid"] == null) {
		echo "You are not logged in.";
		echo "<a href=\"login.php\">Log in</a>";
		exit ();
	}
	require 'connectdb.php';
	global $mysqli;
	if ($down == 0 && $up == 0) {
		$stmt = $mysqli->prepare ( "insert into rate (down,post_id) values (?,?)" );
	} else {
		$stmt = $mysqli->prepare ( "update rate set down=? where post_id=?" );
	}
	$down ++;
	$stmt->bind_param ( 'ii', $down, $post_id );
	$stmt->execute ();
	$stmt->close ();
	header ( "Location: view.php" . "?post_id=" . $post_id );
}

?>



	<!-- show comments -->
<?php
$stmt = $mysqli->prepare ( "select id,comments.user_id, user.username, comment, comments.create_date from comments join user on (user.user_id=comments.user_id) 
		where comments.is_delete=0 and post_id=?" );
$stmt->bind_param ( 'i', $post_id );
$stmt->execute ();
$stmt->bind_result ( $cid, $uid, $cusr, $comment, $cdate );
echo "<hr>";
echo "Comments: <br>";

while ( $stmt->fetch () ) {
	
	echo "<p>" . $comment . "</p>";
	echo "Commented by: " . $cusr . "<br>";
	echo "Commented on: " . substr ( $cdate, 0, 4 ) . "/" . substr ( $cdate, 4, 2 ) . "/" . substr ( $cdate, 6, 2 );
	
	show_comment_delete ( $uid, $cuid, $post_id, $cid );
	echo "<hr>";
}
$stmt->close ();
function show_comment_delete($u, $cu, $pid, $cid) {
	if ($u == $cu) {
		echo "<br><a href=\"editcmt.php?cid=" . $cid . "\">" . "Manage my comment" . "</a>";
	}
}
?>


<!-- new comment -->
<?php
$cm_empty = true;
$cmErr = "";

if (isset ( $_POST ["submit"] )) {
	if ($_SESSION ["uid"] == null) {
		echo "You are not logged in.";
		echo "<a href=\"login.php\">Log in</a>";
		exit ();
	}
	include 'functions.php';
	require 'connectdb.php';
	global $mysqli;
	$cm_empty = is_empty ( $_POST ["comment_text"] );
	
	if ($cm_empty) {
		$cmErr = "Please comment something";
	} else {
		$text = $_POST ["comment_text"];
	}
	if (! $cm_empty) {
		// insert new comment to database
		$stmt = $mysqli->prepare ( "insert into comments (user_id, post_id, comment, create_date) values (?, ?, ?, ?)" );
		date_default_timezone_set ( 'America/Chicago' );
		$date = date ( "Ymd" );
		$text=$mysqli->real_escape_string($text);
		
		if (! $stmt->bind_param ( 'iisi', $cuid, $post_id, $text, $date )) {
			printf ( "Query Bind Failed: %s\n", $mysqli->error );
			exit ();
		}
		if (! $stmt->execute ()) {
			printf ( "Query exe Failed: %s\n", $mysqli->error );
			exit ();
		}
		$stmt->close ();
		// header("Location: index.php");
		
		header ( "Location: view.php" . "?post_id=" . $post_id );
	}
}

?>

<?php if ($cuid == $uid && !$cm_empty) {?>
		<form method="POST"
		action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]."?post_id=".$post_id); ?>">
		<button type="submit" name="delete" value="delete">Delete2</button>
		<br>
		<input type="hidden"
	name="token" value="<?php echo $_SESSION['token'];?>" />
	</form>
	<?php }?>

<form method="POST"
		action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]."?post_id=".$post_id); ?>">
		<textarea name="comment_text" rows="10" cols="30"></textarea>
		<br> <span class="error">*<?php echo $cmErr;?></span>
		<button type="submit" name="submit" value="submit">Comment</button>
		<br>
<input type="hidden"
	name="token" value="<?php echo $_SESSION['token'];?>" />
	</form>


	<a href="index.php">Back to Home</a>
</body>
</html>