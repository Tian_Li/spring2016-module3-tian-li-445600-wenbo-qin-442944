<?php 
ob_start ();
session_start ();
include 'functions.php';
global $token;
if(isset($_POST["logout"])){
	if($_SESSION['token'] !== $_POST['token']){
		die("\n Request forgery detected");
	}
	session_destroy();
	header("Location: login.php");
}
if(isset($_POST["newpost"])){
	if($_SESSION['token'] !== $_POST['token']){
		die("\n Request forgery detected");
	}
	header ( "Location: new_post.php");
}
?>

<!DOCTYPE html>
<html>
<head>
<title>Welcome!</title>
</head>
<body>
	<h1>Welcome <?php echo $_SESSION ["username"]?></h1>
	<form method="POST"
		action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
		<button type="submit" name="logout" value="submit">Log Out</button>
		<button type="submit" name="newpost" value="newpost">New Post</button>
		<input type="hidden"
	name="token" value="<?php echo $_SESSION['token'];?>" />
	</form>
	<?php if ($_SESSION ["uid"] == null) {?>
	<a href="login.php">Log In</a>
	<a href="signup.php">Sign Up</a>
	<?php }?>
	<hr>
	<?php require 'postslist.php';?>
	<!-- <iframe src="postslist.php" width="500" height=auto></iframe> -->

</body>
</html>