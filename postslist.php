<?php
session_start ();
require 'connectdb.php';
global $mysqli;
$stmt = $mysqli->prepare("select post_id, title, user.username, story.create_date 
		from story join user on (story.user_id=user.user_id) where story.is_delete=0");
if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}

$stmt->execute();
$stmt->bind_result($post_id, $title, $username, $date);
echo "<ul>\n";
while($stmt->fetch()){
	echo "<a href=\"view.php?post_id=".$post_id."\">".$title."</a>";
	echo "<br> Author: ".$username."<br>";
}
echo "</ul>\n";
$stmt->close;
?>
