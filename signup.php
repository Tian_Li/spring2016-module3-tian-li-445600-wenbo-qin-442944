<?php
ob_start ();
session_start ();
if($_SESSION['token']==null){
	$_SESSION ['token'] = substr ( md5 ( rand () ), 0, 10 );
}
?>
<!DOCTYPE HTML>
<html>
<head>
<title>Sign up</title>
<style type="text/css">
.error {
	color: #FF0000;
}
</style>
</head>
<body>

<?php
// session_destroy();
$redirect_page = "index.php";
$redirect = false;

$username = $tmpusername = "";
$unErr = $pwdErr = $cpwErr = $emErr = "";
$name_available = $ckpd = $ckem = false;
$name_empty=$pwd_empty=true;
if ($_SERVER ["REQUEST_METHOD"] == "POST") {
	
	include 'functions.php';
	require 'connectdb.php';
	if($_SESSION['token'] !== $_POST['token']){
		die("\n Request forgery detected");
	}
	$name_empty=is_empty($_POST["username"]);
	$pwd_empty=is_empty($_POST["password"]);
	if ($name_empty) {
		$unErr = "Please enter your username.";
	} else {
		// Check username
		$tmpusername = test_input ( $_POST ["username"] );
		if (! preg_match ( "/^\w+$/", $_POST ["username"] )) {
			$unErr = "Only letters, numbers, and underline allowed";
		} else {
			$username = test_input ( $_POST ["username"] );
		}
		
		// Check password
		if ($pwd_empty) {
			$pwErr = "Please enter your password.";
		} else if ($_POST ["password"] != $_POST ["cpassword"]) {
			$pwErr = $cpwErr = "Passwords do not match";
		} else {
			$password = $_POST ["password"];
			$ckpd = true;
		}
		
		// Check E-Mail
		if (! empty ( $_POST ["email"] )) {
			if (! is_email ( $_POST ["email"] )) {
				$emErr = "Please enter a valid E-Mail address.";
			} else {
				$email_address = $_POST ["email"];
				$ckem = true;
			}
		} else {
			$email_address = null;
			$ckem = true;
		}
		//echo "Before check, username= ". $username."<br>";
		
		//check name available
		$name_available = check_available ( $username );
		if ($name_available == false) {
			$unErr = "user already exist";
			echo $name_available;
		}
		
		//create a new user
		if ((!$name_empty) && $ckpd == true && $ckem == true && $name_available == true) {
			new_user ( $username, $password, $email_address );
			$_SESSION ['token'] = substr ( md5 ( rand () ), 0, 10 );
			$_SESSION ["username"] = $username;
			$_SESSION["uid"]=get_uid($username);
			header("Location: ".$redirect_page);
		}
	}
}
function is_email($email) {
	if (filter_var ( $email, FILTER_VALIDATE_EMAIL )) {
		return true;
	} else {
		return false;
	}
}
function new_user($username, $password, $email) {
	global $mysqli;
	$stmt = $mysqli->prepare ( "insert into user (username, password, create_date, email_address) values (?, ?, ?, ?)" );
	if (! $stmt) {
		printf ( "Query Prep Failed: %s\n", $mysqli->error );
		exit ();
	}
	$user = $username;
	$pw = crypt ( $password );
	echo "erypted pwd: ".$pw;
	date_default_timezone_set('America/Chicago');
	$date = date ( "Ymd" );
	$eadd = $email;
	$user=$mysqli->real_escape_string($user);
	$eadd=$mysqli->real_escape_string($eadd);
	if(!$stmt->bind_param ( 'ssis', $user, $pw, $date, $eadd )){
		echo "<br> bind fails<br>";
		printf ( "Query Prep Failed: %s\n", $mysqli->error );
	}
	if(!$stmt->execute ()){
		echo "<br> execute fails <br>";
		printf ( "Query Prep Failed: %s\n", $mysqli->error );
	}
	$stmt->close ();
	echo "new_user";
}
?>

<form method="POST"
		action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
		Username: <input type="text" name="username"
			value="<?php echo $tmpusername; ?>"> <span class="error">*<?php echo $unErr; ?></span>
		<br> Password: <input type="password" name="password"> <span
			class="error">*<?php echo $pwErr; ?></span> <br> Confirm Password: <input
			type="password" name="cpassword"> <span class="error">*<?php echo $cpwErr; ?></span>
		<br> E-Mail: <input type="text" name="email"> <span class="error"><?php echo $emErr; ?></span>
		<br> <input type="hidden" name="token"
			value="<?php echo $_SESSION['token']; ?>" />

		<button type="submit" name="submit" value="submit">Sign Up</button>
	</form>


</body>
</html>