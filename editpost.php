<?php
ob_start ();
session_start ();
if ($_SESSION ["uid"] == null) {
	echo "You are not logged in.";
	echo "<a href=\"login.php\">Log in</a>";
	exit ();
}
require 'connectdb.php';
include 'functions.php';
$post_id = $_GET ["post_id"];
$cuid = $_SESSION ["uid"];

if ($_SERVER ["REQUEST_METHOD"] == "POST") {
	if ($_SESSION ['token'] !== $_POST ['token']) {
		die ( "\n Request forgery detected" );
	}
}

$stmt = $mysqli->prepare ( "select title, story, link from story where post_id=?" );
$stmt->bind_param ( 'i', $post_id );
$stmt->execute ();
$stmt->bind_result ( $title, $story, $link );
$stmt->fetch ();
$stmt->close ();

$em_title = is_empty ( $_POST ["title"] );
$em_po = is_empty ( $_POST ["post_text"] );
$em_lk == is_empty ( $_POST ["link"] );
if (isset ( $_POST ["update"] )) {
	if ($em_title) {
		$ttErr = "Please enter title";
	} else {
		$title = $_POST ["title"];
	}
	if ($em_po) {
		$poErr = "Please enter a story";
	} else {
		$story = $_POST ["post_text"];
	}
	if ($em_lk) {
		$lkErr = "Please enter a link";
	} else {
		$link = $_POST ["link"];
	}
	
	if ((! $em_title) && (! $em_po) && (! $em_lk)) {
		
		
		global $mysqli;
		$stmt = $mysqli->prepare ( "update story set title=?, story=?, link=? where post_id=?" );
		if (! $stmt) {
			printf ( "Query Prep Failed: %s\n", $mysqli->error );
			exit ();
		}
		$title=$mysqli->real_escape_string($title);
		$story=$mysqli->real_escape_string($story);
		$link=$mysqli->real_escape_string($link);
		if(!$stmt->bind_param ( 'sssi', $title, $story, $link, $post_id )){
			printf ( "Query bind Failed: %s\n", $mysqli->error );
		exit ();
		}
		$stmt->execute ();
		$stmt->close ();
		header("Location: view.php"."?post_id=".$post_id);
	}
}

if (isset ( $_POST ["delete"] )) {
	global $mysqli;
	$stmt = $mysqli->prepare ( "update story set is_delete=1 where post_id=?" );
	
	$stmt->bind_param ( 'i', $post_id );
	$stmt->execute ();
	$stmt->close ();
	$stmt = $mysqli->prepare ( "update comments set is_delete=1 where id=?" );
	
	$stmt->bind_param ( 'i', $post_id );
	$stmt->execute ();
	$stmt->close ();
	header ( "Location: index.php");
}

?>

<!DOCTYPE HTML>
<html>
<head>
<title>Edit Story</title>
</head>


<body>
<?php

$url = "editpost.php?post_id=" . $post_id;
// echo "comment= ".$comment;
?>
<form action="<?php echo $url;?>" method="post">
		Title: <input type="text" name="title" value="<?php echo $title; ?>"><span
			class="error">*<?php echo $ttErr;?></span> <br> Post:
		<textarea name="post_text" rows="10" cols="30"><?php echo $story;?></textarea>
		<br> <span class="error">*<?php echo $poErr;?></span> <input
			type="text" name="link" value="<?php echo $link;?>"><span
			class="error">*<?php echo $lkErr;?></span> <input type="hidden"
			name="token" value="<?php echo $_SESSION['token']; ?>" /> <br>
		<button type="submit" name="update" value="update">Update</button>

		<button type="submit" name="delete" value="delete">Delete</button>

	</form>

</body>

</html>