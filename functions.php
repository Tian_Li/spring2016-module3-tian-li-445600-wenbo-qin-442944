<?php
$token;

function test_input($data) {
	$data = trim ( $data );
	$data = stripcslashes ( $data );
	$data = htmlspecialchars ( $data );
	return $data;
}

function check_exist($data) {
	global $mysqli;
	$stmt = $mysqli->prepare ( "select user_id from user where username=?" );
	$data=$mysqli->real_escape_string($data);
	$stmt->bind_param ( 's', $data );
	$stmt->execute ();
	$stmt->bind_result ( $tun );

	if ($stmt->fetch ()) {
		return true;
	} else {
		return false;
	}
}

function check_pwd($name, $pwd_guess) {
	global $mysqli;
	$stmt = $mysqli->prepare ( "SELECT password FROM user WHERE username=?" );
	$name=$mysqli->real_escape_string($name);
	$pwd_guess=$mysqli->real_escape_string($pwd_guess);
	$stmt->bind_param ( 's', $name );
	$stmt->execute ();
	$stmt->bind_result ( $pwd_hash );
	$stmt->fetch ();
	if (crypt ( $pwd_guess, $pwd_hash ) == $pwd_hash)
		return true;
		else
			return false;
}

function check_available($data) {
	global $mysqli;
	$stmt = $mysqli->prepare ( "select user_id from user where username=?" );
	if (! $stmt) {
		printf ( "Query Prep Failed: %s\n", $mysqli->error );
		exit ();
	}
	$data=$mysqli->real_escape_string($data);
	$stmt->bind_param ( 's', $data );
	$stmt->execute ();
	$stmt->bind_result($tun);
	$stmt->fetch();

	if (!$stmt->fetch()) {
		$stmt->close ();
		return true;
	} else {
		$stmt->close ();
		return false;
	}
}

function get_uid($data){
	global $mysqli;
	$stmt=$mysqli->prepare("select user_id from user where username=?");
	$data=$mysqli->real_escape_string($data);
	$stmt->bind_param('s',$data);
	$stmt->execute();
	$stmt->bind_result($uid);
	$stmt->fetch();
	return $uid;
	
}

function is_empty($data){
	if(empty($data))
		return true;
	else
		return false;
}
?>