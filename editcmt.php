<?php
ob_start();
session_start ();
if ($_SESSION ["uid"] == null) {
	echo "You are not logged in.";
	echo "<a href=\"login.php\">Log in</a>";
	exit ();
}
require 'connectdb.php';
include 'functions.php';
$cid= $_GET ["cid"];
$cuid = $_SESSION ["uid"];

global $mysqli;

$stmt=$mysqli->prepare("select comment, post_id from comments where id=?");

$stmt->bind_param('i', $cid);
$stmt->execute();
$stmt->bind_result($comment,$post_id);
$stmt->fetch();
$stmt->close();


$em=is_empty($_POST["cmt"]);
if ($_SERVER ["REQUEST_METHOD"] == "POST") {
	if ($_SESSION ['token'] !== $_POST ['token']) {
		die ( "\n Request forgery detected" );
	}
}

if (isset($_POST["update"])) {
	if ($em) {
		$cmErr = "Please comment something";
	} else {
		$text = $_POST ["cmt"];
	}
	if (! $em) {
		// insert new comment to database
		$stmt = $mysqli->prepare ( "update comments set comment=? where id=?" );
		$text=$mysqli->real_escape_string($text);
		$stmt->bind_param ( 'si', $text, $cid );
		$stmt->execute ();
		$stmt->close ();
		// header("Location: index.php");
		
		//header("Location: view.php"."?post_id=".$post_id);
		
	}
}

if(isset($_POST["delete"])){
	$stmt = $mysqli->prepare ( "update comments set is_delete=1 where id=?" );
	
	$stmt->bind_param ( 'i', $cid );
	$stmt->execute ();
	$stmt->close ();
	header("Location: view.php"."?post_id=".$post_id);
}


?>

<!DOCTYPE HTML>
<html>
<head>
<title>
Edit Comment
</title>
</head>


<body>
<?php $url="editcmt.php?cid=".$cid;
//echo "comment= ".$comment;
?>
<form action="<?php echo $url;?>" method="post">
<textarea name="cmt" rows="10" cols="30" ><?php echo $comment;?></textarea>
<button type="submit" name="update" value="update">Update</button>
<button type="submit" name="delete" value="delete">Delete</button>
<input type="hidden" name="token" value="<?php echo $_SESSION['token'];?>" />
</form>

</body>

</html>