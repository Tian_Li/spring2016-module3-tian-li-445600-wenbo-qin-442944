<?php
ob_start();
session_start ();

$title_empty = $post_empty = true;
$ttErr = $poErr = $lkErr="";
$uid = $_SESSION ["uid"];
if($_SESSION["uid"]==null){
	echo "You are not logged in.";
	echo "<a href=\"login.php\">Log in</a>";
	exit;
}
if ($_SERVER ["REQUEST_METHOD"] == "POST") {
	
	if($_SESSION['token'] !== $_POST['token']){
		die("\n Request forgery detected");
	}
	
	include 'functions.php';
	require 'connectdb.php';
	global $mysqli;
	$title_empty = is_empty ( $_POST ["title"] );
	$post_empty = is_empty ( $_POST ["post_text"] );
	$link_empty=is_empty($_POST["link"]);
	if ($title_empty) {
		$ttErr = "Please enter a title";
	} else {
		$title = $_POST ["title"] ;
	}
	
	if ($post_empty) {
		$poErr = "Please enter some story";
	} else {
		$text = $_POST ["post_text"] ;
	}
if ($link_empty) {
		$lkErr = "Please enter a reference link";
	} else {
		$link =  $_POST ["link"] ;
	}
	if ((! $title_empty) && (! $post_empty)&&(!$link_empty)) {
		
		// insert new post to database
		$stmt = $mysqli->prepare ( "insert into story (title, story, user_id, create_date,link) values (?, ?, ?, ?,?)" );
		
		date_default_timezone_set ( 'America/Chicago' );
		$date = date ( "Ymd" );
		$title=$mysqli->real_escape_string($title);
		$text=$mysqli->real_escape_string($text);
		$link=$mysqli->real_escape_string($link);
		if(!$stmt->bind_param ( 'ssiis', $title, $text, $uid, $date, $link )){
			printf("Query Bind Failed: %s\n", $mysqli->error);
			exit;
		}
			
		if(!$stmt->execute ()){
			printf("Query exe Failed: %s\n", $mysqli->error);
			exit;
		}
		$stmt->close ();
		header("Location: index.php");
	}
}

?>
<!DOCTYPE HTML>
<html>
<head>
<title>New Post</title>
</head>

<body>


	<form method="POST"
		action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
		Title: <input type="text" name="title" value="<?php echo $title; ?>"><span
			class="error">*<?php echo $ttErr;?></span> <br> Post:<br>
		<textarea name="post_text" rows="10" cols="30"></textarea>
		<br> <span class="error">*<?php echo $poErr;?></span><br>
		<input type="text" name="link" value="<?php echo $link;?>"><span
			class="error">*<?php echo $lkErr;?></span>
		<input
			type="hidden" name="token" value="<?php echo $_SESSION['token']; ?>" />
		<br>
		<button type="submit" name="submit" value="submit">Submit</button>
	</form>
	<a href="index.php">Back to Home</a>
</body>
</html>