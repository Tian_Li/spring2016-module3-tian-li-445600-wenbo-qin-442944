<?php
ob_start ();
session_start ();

if (! isset ( $_SESSION ['token'] )) {
	
	$_SESSION ['token'] = substr ( md5 ( rand () ), 0, 10 );
	if (isset ( $_POST ['token'] ))
		$_POST ['token'] = $_SESSION ['token'];
}
?>
<!DOCTYPE HTML>
<html>
<head>
<title>Welcome To 503</title>
<style type="text/css">
.error {
	color: #FF0000;
}
</style>
</head>
<body>

	<?php
	$redirect_page = "index.php";
	$redirect = false;
	$username = $tmpusername = $password = "";
	$unErr = $pwErr = "";
	$name_exist = false;
	$name_empty = $pwd_empty = true;
	
	if ($_SERVER ["REQUEST_METHOD"] == "POST") {
		if ($_SESSION ['token'] !== $_POST ['token']) {
			
			die ( "\n Request forgery detected" );
		}
		include 'functions.php';
		require 'connectdb.php';
		if (isset ( $_POST ["signup"] )) {
			header ( "Location: signup.php" );
		}
		$name_empty = is_empty ( $_POST ["username"] );
		$pwd_empty = is_empty ( $_POST ["password"] );
		if ($name_empty) {
			$unErr = "Please enter your username.";
		} else if ($pwd_empty) {
			$pwErr = "Please enter your password";
		} else {
			$password = $_POST ["password"];
			$tmpusername = test_input ( $_POST ["username"] );
			if (! preg_match ( "/^\w+$/", $_POST ["username"] )) {
				$unErr = "Only letters, numbers, and underline allowed";
			} else {
				$username = test_input ( $_POST ["username"] );
				$name_exist = check_exist ( $username );
				// echo "<h1>用户是否存在" . $name_exist . "</h1>";
				// echo "<br> username= ".$username." password= ".$password."<br>";
				if (! $name_exist) {
					$unErr = "username does not exist.";
					session_destroy ();
				} else {
					// echo "准备检测密码";
					if (! check_pwd ( $username, $password )) {
						$pwErr = "Password is not correct.";
					} else {
						
						$_SESSION ["username"] = $username;
						$_SESSION ["uid"] = get_uid ( $username );
						header ( "Location: " . $redirect_page );
					}
				}
			}
		}
	}
	
	?>

	<form method="POST"
		action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
		Username: <input required type="text" name="username"
			value="<?php echo $tmpusername;?>"> <span class="error">*<?php echo $unErr;?></span>
		<br> Password: <input type="password" name="password"> <span
			class="error">*<?php echo $pwErr;?></span> <br> <input type="hidden"
			name="token" value="<?php echo $_SESSION['token'];?>" />


		<button type="submit" name="submit" value="submit">Log In</button>
		<br>

		<button type="submit" name="signup" value="signup">Sign Up</button>


	</form>
	<p>Show me a Video</p>
	<iframe width="560" height="315" src="https://www.youtube.com/embed/llUtLaklAR4"></iframe>

</body>
</html>